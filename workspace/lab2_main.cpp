#include <iostream>
using namespace std;

class apilacion{

    private:
        /* Atributos */
        int contador = 0;
        int max = 0;
        int tope = -1;
        int *datos = NULL;
        bool band = false;

    public:
        /* Constructor */
        apilacion(int max){
            this -> max = max-1;
            this -> datos = new int[max];
        }

        /* Función: verifica si la pila esta vacia  */
        bool pila_vacia(){
            if (tope == -1){
                band = true;
            }else{
                band = false;
            }
            return band;
        }

        /* Función: verifica si la pila esta llena */
        bool pila_llena(){
            if (tope == max){
                band = true;
            }else{
                band = false;
            }
            return band;
        }

        /* Función: añade un elemento a la pila si es que no esta llena */
        void push(int dato){
            if (pila_llena()){
                cout << "Desbordamiento, pila llena " << endl;
            }else{
                tope = tope + 1;
                datos[tope] = dato;
                cout << "Valor añadido a la pila: " << dato << endl;
            }           
        }

        /* Función: elimina el elemento tope de la pila si es que no esta vacia */
        void pop(){
            if (pila_vacia()){
                cout << "Subdesbordamiento, pila vacı́a " << endl;
            }else{
                int var_removido = datos[tope];
                cout << "Valor removido de la pila: " << var_removido << endl;
                datos[tope] = -1;
                tope = tope - 1;
            }  
        }

        /* Función: imprime los elementos apilados*/
        void imprimir_pila(){
            for (int i = max; i > -1; i--){
                if (datos[i] > 0){
                    cout << "|" << datos[i] << "|" << endl;
                }else {
                    contador++;
                } 
            }
            if (contador > max){
                cout << "No hay elementos apilados (Solo naturales)" << endl;
            }
            contador = 0;
        }
};

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función menu: metodos principales */
string menu_principal (string opt) {
    cout << "========= Menu =========" << endl;
    cout << "Agregar / push  ____ [1]" << endl;
    cout << "Remover / pop  _____ [2]" << endl;
    cout << "Ver pila ___________ [3]" << endl << endl;
    cout << "Salir del programa _ [0]" << endl;
    cout << "========================" << endl;
    cout << "Opción: ";

    cin >> opt;
    
    clear();

    return opt;
}

/* Función para constatar la capacidad de la pila */
int usuario_max_pila () {
    int max;

    cout << "Indique la capacidad máxima de la pila: ";
    cin >> max;
    
    clear();

    return max;
}

/* Funcion main */
int main() {
    //Preguntar la capacidad de la pila al usuario
    int cap_pila = usuario_max_pila();

    //Instanciar pila
    apilacion pila = apilacion(cap_pila);

    //Instanciar variables
    string option = "\0";
    int dato = 0;

    //INICIO DEL MENÚ
    while (option != "0") {
        //llamada al menu principal
        option = menu_principal(option);

        //Primera opción: agregar nuevo elemento tope a la pila
        if (option == "1") {
            cout << "Indique el valor que agregará a la pila (Números naturales): ";
            cin >> dato;
            
            pila.push(dato);
        }
        
        //Segunda opción: eliminar elemento tope de la pila
        if (option == "2") {
            pila.pop();
        }

        //Tercera opción: imprimir en la terminal la apilacion de elementos
        if (option == "3") {
            pila.imprimir_pila();
        }
    }

    return 0;
}